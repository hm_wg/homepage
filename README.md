# Working Group on Hybrid Modelling (HM) in Water and Wastewater Treatment
Welcome to the gitlab of the Working Group on Hybrid Modelling (HM) in Water and Wastewater Treatment.
We are a part of the international water association's (IWA) specialist group on modelling and integrated assessment http://iwa-mia.org/task-groups-working-groups/#HM
The HM Working Group aims to advance hybrid models () and improve water and wastewater system management. The objectives of this working group include, but are not limited to:
- Develop and promote good modelling practices for hybrid modelling as well as bringing hybrid modellers together.
- Collect a open-source list of examples of hybrid models. https://gitlab.com/hm_wg/examples
- Disseminate the latest work within the field: Regularly sharing advancements in HM applications.
- Facilitate idea exchange: Building a network for sharing ideas and feedback.
- Identify knowledge gaps: Addressing deficiencies in current HM practices, such as data quality and metadata issues in HM applications, and promoting good modeling practices for hybrid models in wastewater treatment.


The latest documentation is hosted online at https://hm_wg.gitlab.io/homepage (not created yet).

## What do we mean with hybrid modelling?
With hybrid model we mean the integration of mechanistic and data-driven modelsto address water and wastewater treatment related challenges. By merging these two approaches, HM aims to optimize system design, operation, and understanding of water and wastewater treatment facilities and the processes within. For details please see Schneider et al. (2022).

## Contact
The management committee of HM comprises experts, researchers, and young water professionals who actively disseminate recent progress through scientific journal publications, reports, workshops, and a platform with examples of hybrid models. These platforms provide opportunities for researchers working on HM to share their research, exchange ideas, and collaborate effectively. Through this approach, HM has successfully published a review article (Schneider et al. 2022) and organized workshops (see [Conference Contributions](#conference-contributions)) at prominent international conferences such as WRRmod 2022+ (Water Resource Recovery Modelling Seminar), Watermatex 2023, WRRmod 2024, and the International Conference on Hydroinformatics (HIC 2024).
Please contact the chair or the vice chair:
- Chair: Mariane Y. Schneider (Ghent University, Belgium)
- Vize-Chair: Jun-Jie Zhu (Princeton University, United States)

## Management committee
- Sina Borzooei (IVL, Sweden)
- Saba Daneshgar (Ghen University, Belgium)
- Ali Reza Dehghani Tafti (Polytechnique Montréal, Canada)
- Andreas Frömelt (Eawag, Switzerland)
- Cristian Gómez Cortés (Ghent University, Belgium)
- Marcello Serrao (Suez, France)
- Jeffrey Sparks (HRSD, United States)
- Elena Torfs (Laval University, Canada)
- Kris Villez (ORNL, United States)
- Xu Zou (The Hong Kong University of Science and Technology, Hong Kong)

## How to contribute
If you have an idea about a webinar, a conference where you would like a session on hybrid modelling, or a suggestion for collaboration, feel free to contact us and become a friend of the HM working group, as the ones listed below already are.

## Friends of the HM working group
These are contributors that are not inside the management committee who contribute to different initiatives.
- Francesca Casagli
- Gayeong Kim
- Loes Verhaeghe
- Florian Wenk

## Former Members of the management committee
We would like to mention all former members who greatly contributed to this initiative.
- Ramesh Saagi (beginning - 2023)
- Ward Quaghebeur (beginning - 2023)
- Matthew Wades (beginning - 2022)
- Feiyi Li (beginning - 2022)

## Publications
### Peer Reviewed Articles
```
@article{schneider:2022,
	title = {Hybrid modelling of water resource recovery facilities: status and opportunities},
	volume = {85},
	issn = {0273-1223},
	shorttitle = {Hybrid modelling of water resource recovery facilities},
	url = {https://doi.org/10.2166/wst.2022.115},
	doi = {10.2166/wst.2022.115},
	number = {9},
	urldate = {2022-05-23},
	journal = {Water Science and Technology},
	author = {Schneider, Mariane Yvonne and Quaghebeur, Ward and Borzooei, Sina and Froemelt, Andreas and Li, Feiyi and Saagi, Ramesh and Wade, Matthew J. and Zhu, Jun-Jie and Torfs, Elena},
	month = apr,
	year = {2022},
	pages = {2503--2524}
}
```

### Conference Contributions
```
@conference{gomez2024hybrid,
title={Hybrid Modeling: Bridging Data-Driven and Mechanistic Approaches for Water Management},
author={Gomez, C. and Zou, X. and Schneider, M.Y. and Zhu, J.J. and Borzooei, S. and Daneshgar, S. and Froemelt, A. and Sparks, J. and Tafti, A.R.D. and Torfs, E. and Villez, K.},
booktitle={15th International Conference on Hydroinformatics - HIC 27.-30.05.2024},
address={Beijing, China},
year={2024},
}
@conference{schneider2024evaluating,
title={Evaluating The Hybrid Modelling Competition: A Step Towards Developing Good Modelling Practice},
author={Schneider, M.Y. and Zhu, J.J. and Borzooei, S. and Daneshgar, S. and Froemelt, A. and Sparks, J. and Tafti, A.R.D. and Torfs, E. and Villez, K. and Zou, X.},
booktitle={WRRmod 2024, 9th IWA Water Resource Recovery Modelling Seminar, 06-10.04.2024},
address={Notre Dame, IN 46556, USA},
year={2024}
}
@conference{torfs2023how,
title={How Can Hybrid Modelling Be Used For Model Complexity Reduction?},
author={Torfs, E. and Fernandes del Pozo, D. and Froemelt, A. and Borzooei, S. and Daneshgar, S. and G{'o}mez Cort{'e}z, C.C. and Saagi, R. and Sparks, J. and Zou, X. and Villez, K. and Schneider, M.Y.},
booktitle={IWA conference Watermatex 2023, 23-27.9.2023},
address={Quebec City, QC, Canada},
year={2023}
}
@conference{borzooei2023how,
title={How to build and use hybrid models for water resource recovery facilities?},
author={Borzooei, S.* and Froemelt, A.* and Gaublomme, D. and Quaghebeur, W. and Saagi, R. and Schneider, M.Y. and Torfs, E. and Villez, K. and Zhu, J.},
booktitle={WRRmod 2022+, 8th IWA Water Resource Recovery Modelling Seminar, 15-18.01.2023},
address={Stellenbosch, South Africa},
year={2023}
}
```
